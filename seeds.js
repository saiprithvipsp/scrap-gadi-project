var mongoose = require("mongoose");
var Campground = require("./models/campground");
var Comment = require("./models/comment");

var data = [
    {
       name:"Winterfell",
       image:"https://source.unsplash.com/Hxs6EAdI2Q8",
       description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
       ,location:"city1"
       ,area: "area1"
    },
    {
       name:"King's Landing",
       image:"https://source.unsplash.com/cTp5fkQch1I",
       description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
       ,location:"city1"
       ,area: "area2"
    },
    {
       name:"Dragon Stone",
       image:"https://source.unsplash.com/p586q6VJvSU",
       description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.h"
       ,location:"city2"
       ,area: "area3"
    }
    ];

function seedDB(){
      Campground.remove({},function(err){
        if(err){
            console.log(err);
        }else{
            console.log("Campgrounds deleted!");
        }
        
        data.forEach(function(seed){
            Campground.create(seed,function(err, campground){
                if(err){
                    console.log(err);
                }else{
                    console.log("Campground created");
                    //create comments
                    Comment.create(
                        {
                        text:"this is a comment",
                        author:"rffvasffv"
                        }, function(err, comment){
                            if(err){
                                console.log(err);
                            }else{
                                campground.comments.push(comment);
                                campground.save();
                                console.log("Created a new comment");
                            }
                        }
                    );
                }
            });
        });
    });  
}
 module.exports = seedDB;
