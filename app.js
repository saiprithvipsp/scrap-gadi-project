var express = require("express"),
 	app = express(),
	bodyparser = require("body-parser"),
	mongoose = require("mongoose"),
	Campground = require("./models/campground"),
	seedDB = require("./seeds"),
	Comment = require("./models/comment"),
	knock = require("knock-knock-jokes"),
	passport = require('passport'),
	localStrategy = require('passport-local'),
	passportLocalMongoose = require('passport-local-mongoose'),
	User = require('./models/users'),
	conn = mongoose.connection;

mongoose.connect("mongodb://localhost:27017/yelp_camp");

seedDB();

app.use(express.static(__dirname+"/public"));

app.use(bodyparser.urlencoded({extended:true}));
app.set("view engine","ejs");

app.use(require('express-session')({
    secret:"this is the secret sentence",
    resave:false,
    saveUninitialized:false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new localStrategy(User.authenticate()));


passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }else{
        res.redirect("/login");
    }
}

app.get("/",function(req,res){
	// var knockKnock = knock();
	res.render("landing");
});

app.get("/campgrounds",function(req,res){
		Campground.find({},function(err,allCampgrounds){
			if(err){
				console.log(err);
			}else{
				allCampgrounds.user = req.user;
				console.log(req.user);
				res.render("campgrounds/index",{campgrounds:allCampgrounds,user:req.user});
			}
		})
	// res.render("campgrounds",{campgrounds:campgrounds});
});

app.post("/search/campgrounds",function(req,res){
	console.log(req.body.location);
	if(req.body.location){
		Campground.find({location:req.body.location},function(err,allCampgrounds){
			if(err){
				console.log(err);
			}else{
				res.render("campgrounds/index",{campgrounds:allCampgrounds,user:req.user});
			}
		});
	}else{
		Campground.find({area:req.body.area},function(err,allCampgrounds){
			if(err){
				console.log(err);
			}else{
				res.render("campgrounds/index",{campgrounds:allCampgrounds,user:req.user});
			}
		});
	}
});

app.post("/campgrounds",function(req,res){
   var name = req.body.name;
   var image = req.body.image;
   var desc = req.body.description;
   var location = req.body.location;
   var newObject = {name:name,image:image,description:desc,location:location};
   
   Campground.create(newObject,function(err,newlyCreated){
	   if(err){
		   console.log(err);
	   }else{
		   res.redirect("/campgrounds");
	   }
   })

});

app.get("/campgrounds/new",function(req, res) {
   res.render("campgrounds/new",{user:req.user});
});

app.get("/campgrounds/:id",function(req,res){
   Campground.findById(req.params.id).populate("comments").exec(function(err,foundCampground){
	  if(err){
		  console.log(err);
	  }else{
			 res.render("campgrounds/show",{foundCampground:foundCampground,user:req.user}); 
	  } 
   });
});

app.get("/campgrounds/delete/:id",isLoggedIn,function(req,res){
	Campground.findOneAndDelete(req.params.id,function(err,campground){
		console.log(campground);
		res.redirect('/campgrounds');
	});
});

//=========COMMENT ROUTES============
app.get("/campgrounds/:id/comments/new",function(req, res) {
	Campground.findById(req.params.id,function(err,campground){
	   if(err){
		   console.log(err);
	   } else{
		   res.render("comments/new",{campground:campground,user:req.user});
	   }
	});
});

app.post("/campgrounds/:id/comments",function(req,res){
   Campground.findById(req.params.id,function(err, campground) {
	   if(err){
		   console.log(err);
		   res.redirect("/campgrounds");
	   }else{
		   Comment.create(req.body.comments,function(err, comment){
			  if(err){
				  console.log(err);
			  } else{
				  campground.comments.push(comment);
				  campground.save();
				  res.redirect("/campgrounds/"+campground._id);
			  }
		   });
	   }
   }) ;
});

//=========== AUTH Routes============

app.get("/register",function(req,res){
    res.render("register",{user:req.user});
})

app.post('/register',function(req,res){
    User.register(new User({username: req.body.username}),req.body.password,function(err, user){
        if(err){
            console.log(err);
            return res.render('/register');
        }
        passport.authenticate("local")(req,res, function(){
            res.redirect('/campgrounds');
        })
    })
})

app.get('/login',function(req,res){
    res.render('login',{user:req.user});
});

app.post('/login',passport.authenticate('local',{
    successRedirect:'/campgrounds',
    failureRedirect:'/login'
}),function(req,res){
});

app.get("/logout",function(req,res){
    req.logout();
    res.redirect("/campgrounds");
});


var port = 3000;
// console.log(process.env.PORT,process.env.IP);

app.listen(port,process.env.IP,function(){
	console.log("Server started!");
});